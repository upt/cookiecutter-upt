#!/bin/bash
sed -i "s/YEAR/$(date +%Y)/" LICENSE

if [[ "{{ cookiecutter.kind }}" == "backend" ]];
then
	mkdir {{ cookiecutter.package_directory_name }}/templates
	touch {{ cookiecutter.package_directory_name }}/templates/base.tmpl
fi

git init .
git add .
git commit -m "Initial commit."
