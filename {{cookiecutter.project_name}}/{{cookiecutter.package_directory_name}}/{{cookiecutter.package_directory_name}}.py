import upt

{% if cookiecutter.kind == 'backend' %}
class {{ cookiecutter.main_class_name }}(upt.Backend):
    name = '{{ cookiecutter.entrypoint_name }}'

    def create_package(self, upt_pkg, output=None):
        pass
{%- else %}
class MyPackage(upt.Package):
    pass


class {{ cookiecutter.main_class_name }}(upt.Frontend):
    name = '{{ cookiecutter.entrypoint_name }}'

    def parse(self, pkg_name):
        version = ''
        return MyPackage(pkg_name, version)
{%- endif %}
