# Cookiecutter template for upt backends/frontends

You may create a new backend/frontend for [upt](https://framagit.org/upt/upt) 
by running:

    $ cookiecutter https://framagit.org/upt/cookiecutter-upt

and answering the questions prompted on the screen. A git repo will be created.
